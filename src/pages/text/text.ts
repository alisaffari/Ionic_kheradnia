import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-text',
  templateUrl: 'text.html',
})
export class TextPage {

  title;
  desc;
  text;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.title = navParams.get('ti');
    this.desc = navParams.get('de');
    this.text = navParams.get('te');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TextPage');
  }

}
