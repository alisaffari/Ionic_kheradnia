import { TextPage } from './../text/text';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Texts } from '../../app/app.constants';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  texts = Texts;

  constructor(public navCtrl: NavController) {

  }

  gototext(title,desc,text) {
    this.navCtrl.push(TextPage,{ti: title, de: desc, te: text});
  }

}
