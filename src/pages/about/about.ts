import { AudioPage } from './../audio/audio';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Audios } from '../../app/app.constants';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  audios = Audios;

  constructor(public navCtrl: NavController) {

  }

  gotoaudio(title,desc,Down,path,url) {
    this.navCtrl.push(AudioPage, {ti: title, de: desc, do: Down, pa: path, url: url});
  }

}
