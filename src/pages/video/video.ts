import { Videos } from './../../app/app.constants';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-video',
  templateUrl: 'video.html',
})
export class VideoPage {

  id;
  video;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.id = navParams.get('id');
    Videos.forEach(item => {
      if (item.id == this.id) {
        this.video = item;
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideoPage');
  }



}
