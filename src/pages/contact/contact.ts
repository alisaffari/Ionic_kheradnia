import { VideoPage } from './../video/video';
import { Videos } from './../../app/app.constants';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  videos = Videos;

  constructor(public navCtrl: NavController) {

  }

  gotovideo(id) {
    this.navCtrl.push(VideoPage, {id: id});
  }

}
