import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-audio',
  templateUrl: 'audio.html',
})
export class AudioPage {

  title;
  desc;
  Down;
  path;
  url;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.title = navParams.get('ti');
    this.desc = navParams.get('de');
    this.Down = navParams.get('do');
    this.path = navParams.get('pa');
    this.url = navParams.get('url');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AudioPage');
  }

}
