import { Injectable } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import {FileTransfer, FileTransferObject} from '@ionic-native/file-transfer';
import {File} from '@ionic-native/file';
import { FileEncryption } from '@ionic-native/file-encryption';
import { Audios, Videos } from '../../app/app.constants';

@Injectable()
export class DataProvider {

  fileTransfer: FileTransferObject = this.transfer.create();

  constructor(public platform: Platform, private transfer: FileTransfer, private file: File, public alertCtrl: AlertController,public filencrypt: FileEncryption) {
    console.log('Hello DataProvider Provider');
    this.InitialCheck();
  }

  Download_Audio(url, name) {
    this.fileTransfer.download(url, this.file.externalCacheDirectory + name, true)
    .then((entry) => {
      Audios.forEach(item => {
        if (item.title == name) {
          item.path = this.file.externalCacheDirectory+name;
          item.Downloaded = true;
        }
      });
      this.filencrypt.encrypt(this.file.externalCacheDirectory+name, 'samsung21');
    })
    .catch((err) => {
      console.log(err);
    });
  }

  Download_Video(url, id) {
    this.fileTransfer.download(url, this.file.externalCacheDirectory + id + '.mp4', true)
    .then((entry) => {
      Videos.forEach(item => {
        if (item.title == name) {
          item.path = this.file.externalCacheDirectory+name;
          item.Downloaded = true;
        }
      });
      this.filencrypt.encrypt(this.file.externalCacheDirectory+name, 'samsung21');
    })
    .catch((err) => {
      console.log(err);
    });
  }

  InitialCheck(){
    Videos.forEach(item => {
      this.file.checkDir(this.file.externalCacheDirectory, item.title)
      .then((val) => {
        item.Downloaded = true;
      })
      .catch((val) => {
        item.Downloaded = false;
      })
    });
    Audios.forEach(item => {
      this.file.checkDir(this.file.externalCacheDirectory, item.title)
      .then((val) => {
        item.Downloaded = true;
      })
      .catch((val) => {
        item.Downloaded = false;
      })
    });
  }

}
